/*
@brief 将特征值存入sqlite数据库
@date 2012-09-03
*/
// SQLiteTest.cpp : Defines the entry point for the console application.
// 

#include "sqlite3.h"
#include "Globle_Variable.h"
#include <windows.h>
#include <iostream>
using namespace std;
sqlite3 * pDB = NULL;
bool SaveNumAndConnect(int num, char *sName, int sConnected);//save feature
static WCHAR *mbcsToUnicode(const char *zFilename);
static char *unicodeToUtf8(const WCHAR *zWideFilename);
static WCHAR *utf8ToUnicode(const char *zFilename);
static char *unicodeToMbcs(const WCHAR *zWideFilename);
char *AnsiToUtf8(const char *zFilename);   /** ansi 2 utf8 */
char *Utf8ToAnsi(char *zFilename);  /*** utf8 - ansi */

int number_char_db()
{
	//打开路径采用utf-8编码
	//如果路径中包含中文，需要进行编码转换

	int nRes = sqlite3_open("D:\\Download\\sqlite\\Chinese_charater.db", &pDB);
	if (nRes != SQLITE_OK)
	{
		cout << "Open database fail: " << sqlite3_errmsg(pDB);
		sqlite3_free(pDB);
		goto QUIT;
	}


	for (int i = 0; i < M; i++){
		SaveNumAndConnect(i+1,filename_train[i], num_of_connection[i]);
	}
QUIT:
	sqlite3_close(pDB);
	return 0;
}

bool SaveNumAndConnect(int num, char* sName, int sConnected)
{
	char strSql[100] = { 0 };
	char* sName_utf8 = AnsiToUtf8(sName);
	sprintf(strSql, "insert into char_id_connections(id,name,connections) values (%d,'%s', %d);", num, sName_utf8, sConnected);
	char* cErrMsg;
	int nRes = sqlite3_exec(pDB, strSql, 0, 0, &cErrMsg);
	if (nRes != SQLITE_OK)
	{
		cout << "add  fail: " << cErrMsg << endl;
		sqlite3_free(cErrMsg);
		return false;
	}
	else
	{
		cout << "add success: " << sName << "\t" << sConnected << endl;
	}
	return true;
}

static WCHAR *mbcsToUnicode(const char *zFilename)
{
	int nByte;
	WCHAR *zMbcsFilename;
	int codepage = AreFileApisANSI() ? CP_ACP : CP_OEMCP;

	nByte = MultiByteToWideChar(codepage, 0, zFilename, -1, NULL, 0)*sizeof(WCHAR);
	zMbcsFilename = (WCHAR*)malloc(nByte*sizeof(zMbcsFilename[0]));
	if (zMbcsFilename == 0){
		return 0;
	}
	nByte = MultiByteToWideChar(codepage, 0, zFilename, -1, zMbcsFilename, nByte);
	if (nByte == 0){
		free(zMbcsFilename);
		zMbcsFilename = 0;
	}
	return zMbcsFilename;
}

static char *unicodeToUtf8(const WCHAR *zWideFilename)
{
	int nByte;
	char *zFilename;

	nByte = WideCharToMultiByte(CP_UTF8, 0, zWideFilename, -1, 0, 0, 0, 0);
	zFilename = (char*)malloc(nByte);
	if (zFilename == 0){
		return 0;
	}
	nByte = WideCharToMultiByte(CP_UTF8, 0, zWideFilename, -1, zFilename, nByte,
		0, 0);
	if (nByte == 0){
		free(zFilename);
		zFilename = 0;
	}
	return zFilename;
}


static WCHAR *utf8ToUnicode(const char *zFilename)
{
	int nChar;
	WCHAR *zWideFilename;

	nChar = MultiByteToWideChar(CP_UTF8, 0, zFilename, -1, NULL, 0);
	zWideFilename = (WCHAR*)malloc(nChar*sizeof(zWideFilename[0]));
	if (zWideFilename == 0){
		return 0;
	}
	nChar = MultiByteToWideChar(CP_UTF8, 0, zFilename, -1, zWideFilename, nChar);
	if (nChar == 0){
		free(zWideFilename);
		zWideFilename = 0;
	}
	return zWideFilename;
}

static char *unicodeToMbcs(const WCHAR *zWideFilename)
{
	int nByte;
	char *zFilename;
	int codepage = AreFileApisANSI() ? CP_ACP : CP_OEMCP;

	nByte = WideCharToMultiByte(codepage, 0, zWideFilename, -1, 0, 0, 0, 0);
	zFilename = (char*)malloc(nByte);
	if (zFilename == 0){
		return 0;
	}
	nByte = WideCharToMultiByte(codepage, 0, zWideFilename, -1, zFilename, nByte,
		0, 0);
	if (nByte == 0){
		free(zFilename);
		zFilename = 0;
	}
	return zFilename;
}
char *AnsiToUtf8(const char *zFilename)   /** ansi 2 utf8 */
{
	char *zFilenameUtf8;
	WCHAR *zTmpWide;

	zTmpWide = mbcsToUnicode(zFilename);
	if (zTmpWide == 0){
		return 0;
	}
	zFilenameUtf8 = unicodeToUtf8(zTmpWide);
	free(zTmpWide);
	return zFilenameUtf8;
}
char *Utf8ToAnsi(char *zFilename)  /*** utf8 - ansi */
{
	char *zFilenameMbcs;
	WCHAR *zTmpWide;

	zTmpWide = utf8ToUnicode(zFilename);
	if (zTmpWide == 0){
		return 0;
	}
	zFilenameMbcs = unicodeToMbcs(zTmpWide);
	free(zTmpWide);
	return zFilenameMbcs;
}