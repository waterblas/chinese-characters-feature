#include "Globle_Variable.h"
#include <stdio.h>
using namespace std;
//#include<math.h>
/****函数connection_detect_database(int mount)********************************************/
/****函数功能是用于检测所有训练样本连通区域的个数***/
/****函数参数mount是训练样本的个数***********/
/*****函数返回值:将所有训练样本连通区域的个数写入全局数组num_of_connection[M]*************/
#define N   10      //区域的象素数门限值
const int strack_size = 7000;
void connection_detect_database(int mount)
{
	int n;
	int num_of_connetion = 0;  //连通区域个数;
	int i, j, k;
	int top = -1;            //指示栈顶
	int *strack_x, *strack_y;
	strack_x = new int[strack_size];
	strack_y = new int[strack_size];
	//int strack_x[1000] = { 0 };      //栈用于存放横坐标
	//int strack_y[1000] = { 0 };     //栈用于存放纵坐标
	int pre_top_x, pre_top_y;        //用于存放刚刚出栈的栈顶元素横纵坐标
	unsigned char connection_detect_array[1000][1000] = { 255 };  //中间数组存放象素值
	/***********借用二值化函数*获得灰度矩阵****************/
	bool lable = false;  //用于判断每个区域的象素数是否达到门限值
	int word_size = mount;
	float deal_rate = 0.1;

	for (k = 0; k < mount; k++)
	{
		num_of_connetion = 0;  //连通区域个数;
		top = -1;
		n = k;   //把第k个文字序号付给n,用于打开图片
		lable = false;
		FILE *p;   //位图文件指针
		string filename_str = filename_head + filename_train[n] + filename_tail;
		if (!(p = fopen(filename_str.c_str(), "rb+")))
			printf("can't not open bmp file");
		fseek(p, 18, 0);
		fread(&width, sizeof(int), 1, p);  //读宽度信息
		//printf("width=%d\n", width);
		fread(&high, sizeof(int), 1, p);   //读高度信息
		//printf("high=%d\n", high);
		fseek(p, 10, 0);
		fread(&gap, sizeof(int), 1, p);    //读数据起始位置信息


		fseek(p, gap, 0);
		/********connection_detect_array[i][j]初始化成背景色********************/
		for (i = 0; i < 1000; i++)
		{
			for (j = 0; j < 1000; j++)
			{
				connection_detect_array[i][j] = 255;
			}
		}

		/***************读灰度值并二值化***********************/
		for (i = 0; i < high; i++)
		{
			for (j = 0; j < width; j++)
			{
				if (feof(p))
				{
					printf("read over!");
					break;
				}
				else
				{
					if (fread(&(gray[i][j]), sizeof(char), 1, p) != 1)
					{
						printf("fatal error!");
						break;
					}
					if (gray[i][j] <= 200)gray[i][j] = 0;
					else gray[i][j] = 255;
				}
			} //end of for(j=0;j<width;j++)

		}  //end of (i=0;i<high;i++)

		/**********借用二值化函数结束***************/

		/********拷贝灰度数组gray值**********/

		for (i = 0; i < high; i++)
		{
			for (j = 0; j < width; j++)
			{

				connection_detect_array[i][j] = gray[i][j];

			} //end of for(j=0;j<width;j++)

		}  //end of (i=0;i<high;i++)
		/************拷贝结束**********************/


		/*****开始统计连通区域个数***********/
		for (i = 0; i < high; i++)
		{
			for (j = 0; j < width; j++)
			{
				top = -1;
				if (connection_detect_array[i][j] == 0)
				{
					top++;
					strack_x[top] = i;
					strack_y[top] = j;
				}
				/***********如果栈不为空判断周围8个象素*********************/
				while (top != (-1))
				{
					if (top >= N) lable = true;
					pre_top_x = strack_x[top];  // 将刚刚出栈的栈顶元素横纵坐标付给临时变量
					pre_top_y = strack_y[top];
					connection_detect_array[pre_top_x][pre_top_y] = 255;   //取出栈顶元素并将其置白;
					top--;  //栈元个数减1	

					if ((connection_detect_array[pre_top_x - 1][pre_top_y - 1] == 0) && (pre_top_x >= 1) && (pre_top_y >= 1))
					{
						top++;
						strack_x[top] = pre_top_x - 1;
						strack_y[top] = pre_top_y - 1;
					}
					if ((connection_detect_array[pre_top_x - 1][pre_top_y] == 0) && (pre_top_x >= 1))
					{
						top++;
						strack_x[top] = pre_top_x - 1;
						strack_y[top] = pre_top_y;
					}

					if ((connection_detect_array[pre_top_x - 1][pre_top_y + 1] == 0) && (pre_top_x >= 1))
					{
						top++;
						strack_x[top] = pre_top_x - 1;
						strack_y[top] = pre_top_y + 1;
					}
					if ((connection_detect_array[pre_top_x][pre_top_y - 1] == 0) && (pre_top_y >= 1))
					{
						top++;
						strack_x[top] = pre_top_x;
						strack_y[top] = pre_top_y - 1;
					}


					if ((connection_detect_array[pre_top_x][pre_top_y + 1] == 0))
					{
						top++;
						strack_x[top] = pre_top_x;
						strack_y[top] = pre_top_y + 1;
					}

					if ((connection_detect_array[pre_top_x + 1][pre_top_y - 1] == 0) && (pre_top_y >= 1))
					{
						top++;
						strack_x[top] = pre_top_x + 1;
						strack_y[top] = pre_top_y - 1;
					}

					if ((connection_detect_array[pre_top_x + 1][pre_top_y] == 0))
					{
						top++;
						strack_x[top] = pre_top_x + 1;
						strack_y[top] = pre_top_y;
					}
					if ((connection_detect_array[pre_top_x + 1][pre_top_y + 1] == 0))
					{
						top++;
						strack_x[top] = pre_top_x + 1;
						strack_y[top] = pre_top_y + 1;
					}
					if (top > strack_size){
						printf("%d  ", top);
						printf("%s \n", filename_train[n]);
					}
				}  //end of  while(top!=-1)

				/***********如果栈不为空判断周围8个象素 结束!*********************/

				if (lable)num_of_connetion++;  //连通区域个数加1
				lable = false;

			} //end of for(j=0;j<width;j++)

		}  //end of (i=0;i<high;i++)


		/***************/
		fclose(p);
		num_of_connection[k] = num_of_connetion;

		if (k >= word_size*deal_rate - 1){
			printf("-");
			deal_rate += 0.1;
		}
		if (k == word_size - 1){
			printf("complete 100%% \n");
		}
	}
	delete[] strack_x;
	delete[] strack_y;

}

