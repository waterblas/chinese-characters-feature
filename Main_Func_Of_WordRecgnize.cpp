#include<stdio.h>
#include<malloc.h>
#include "Globle_Variable.h"
#include"Function_Declare.h"
using namespace std;
int width;  //图像宽度(象素点数)
int high;   //图像高度(象素点数)
unsigned char gray[1000][1000] = { 255 };
unsigned char reserve[1000][1000] = { 255 };     //保存二值化后的象素值
unsigned char corrdinate_to_48[48][48] = { 255 };   //保存转换成48*48图片后的象素值
float percent[16];  //保存corrdinate_to_48中投影围特征各行列的百分比
float percent_blanks[32];  //保存corrdinate_to_48中粗外围特征各行列的百分比
int total_piexls;   //corrdinate_to_48中总象素数
int total_blanks;;   //corrdinate_to_48中总空白点数
int gap;   //数据起始位置
int num_of_connection[M];  //所有样本文字连同区域的总数量
int left, right, top, bottom;
/********************训练样本数据**************************************/
char filename_train[M][5];
int database_total[M] = { 0 }; //用于保存所有训练样本的总象素数
float database_percent[M][16] = { 0.0 };  //用于保存所有训练样本的投影象素百分比
float database_percent_blanks[M][32] = { 0.0 };  //用于保存所有训练样本的粗外围特征各行列空白百分比
string filename_tail = ".bmp";
string filename_head = ".\\bmp2\\"; //bmp文件存放位置
char *filename = ".\\data.txt"; //6762个汉字文本
int N;
void save_feature_txt();
int main()
{
	printf("start...\n");
	FILE *fp;
	fp = fopen(filename, "rt");
	if (fp == NULL) {
		printf("不能打开数据文件:%s", filename);
		return 1;
	}
	for (int i = 0; i < M; i++) {
		fscanf(fp, "%s", filename_train[i]);
		printf("%s", filename_train[i]);
		if (feof(fp))
		{
			printf("\n read over! There are %d words", i);
			break;
		}
	}
	fclose(fp);
	/*=======================训练过程==========================*/
	/*^^^^^^^^^^^^^^^^^^^^^第一步^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
	/*----对训练样本文字进行连通区域检测,并把检测结果储存------*/
	printf("\n first step:calculate number of connected domains...\n");
	connection_detect_database(M);

	/*^^^^^^^^^^^^^^^^^^^^^第二步^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
	/*--对训练样本文字投影特征进行分格统计,并把结果储存-------*/
	printf("\n second step :calculate features...\n");
	initial_database();
	
	/*将投影和粗外围特征值写入文件*/
	save_feature_txt();
	
	number_char_db();
	return 0;
}
void save_feature_txt()
{
	FILE *shadow_f;
	shadow_f = fopen(".\\features\\shadow.txt", "w");
	if (shadow_f == NULL) {
		printf("不能打开数据文件:shadow.txt");
	}
	else{
		for (int i = 0; i < M; i++)
		{
			fprintf(shadow_f, "%d ", i + 1);
			for (int j = 0; j < 16; j++)
			{
				fprintf(shadow_f, "%f ", database_percent[i][j]);
			}
			fprintf(shadow_f, "\n");
		}
		fclose(shadow_f);
	}
	FILE *rough_f;
	rough_f = fopen(".\\features\\rough.txt", "w");
	if (rough_f == NULL) {
		printf("不能打开数据文件:rough.txt");
	}
	else{
		for (int i = 0; i < M; i++)
		{
			fprintf(rough_f, "%d ", i + 1);
			for (int j = 0; j < 32; j++)
			{
				fprintf(rough_f, "%f ", database_percent_blanks[i][j]);
			}
			fprintf(rough_f, "\n");
		}
		fclose(rough_f);
	}
}