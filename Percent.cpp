#include "Globle_Variable.h"
#include <stdio.h>
/****函数Func_percent(float per[])*********************/
/****函数功能是利用当前的全局数组corrdinate_to_48[][]对当前的样本进行8*8行列
 *************** 在每个行列计算象素百分比************/
/*****函数返回值:每个8*8行列计算象素百分比,8行8列共16个,
存入全局数组database_percent[M][]中***********************/
int Func_percent(float per[])
{

	int i, j;
	int num = 0;  //用于统计每8行(列)内的象素数;

	/*********初始化***************/

	for (i = 0; i < 16; i++)
	{
		per[i] = 0.0;
	}
	/************************/
	/******************计算corrdinate_to_48中总象素数*************/
	total_piexls = 0;
	for (i = 0; i < 48; i++)
	{

		for (j = 0; j < 48; j++)
		{

			if (corrdinate_to_48[i][j] == 0)

			{
				total_piexls++;

			}

		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	/******************计算corrdinate_to_48中各行列象素数百分比,先行后列*************/
	/***********行统计****************/
	for (i = 0; i < 48; i++)
	{
		if ((i != 0) && (i % 6 == 0))
		{
			per[i / 6 - 1] = (float)num;
			num = 0;
		}
		for (j = 0; j < 48; j++)
		{

			if (corrdinate_to_48[i][j] == 0)

			{
				num++;

			}

		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	per[7] = (float)num; num = 0;
	/***********行统计完毕***********/
	/***********列统计****************/
	for (i = 0; i < 48; i++)
	{
		if ((i != 0) && (i % 6 == 0))
		{
			per[i / 6 + 7] = (float)num; num = 0;
		}
		for (j = 0; j < 48; j++)
		{

			if (corrdinate_to_48[j][i] == 0)

			{
				num++;

			}

		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	per[15] = (float)num; num = 0;
	/***********列统计完毕***********/
	/**************计算百分比**********/
	for (i = 0; i < 16; i++)
	{
		per[i] = per[i] * 100 / total_piexls;
	}
	/*******检测出疑似横竖的情况,加权减半***
	 for(i=0;i<16;i++)
	 {
	 if(per[i]>=10)
	 {
	 per[i]=per[i]/2;
	 }
	 }
	 ***/

	return total_piexls;
}