#include "Globle_Variable.h"
#include <stdio.h>
using namespace std;
/****函数two_value_train(int n)*********************/
/****函数功能是用于对某个训练样本二值化*************/
/****函数参数n指明训练样本的序号********************/
/*****函数返回值:二值化后的象素值写入全局数组gray[][]中,并拷贝到全局数组reserve[][]中***********************/
void two_value_train(int n)
{
	int i, j;
	FILE *p;   //位图文件指针
	string filename_str = filename_head + filename_train[n] + filename_tail;
	if (!(p = fopen(filename_str.c_str(), "rb+")))
	{
		printf("can't not open bmp file");
		return;
	}	
	fseek(p, 18, 0);
	fread(&width, sizeof(int), 1, p);  //读宽度信息
	//printf("width=%d\n",width);
	fread(&high, sizeof(int), 1, p);   //读高度信息
	//printf("high=%d\n",high);
	fseek(p, 10, 0);
	fread(&gap, sizeof(int), 1, p);    //读数据起始位置信息
	fseek(p, gap, 0);


	/***************读灰度值并二值化***********************/

	for (i = 0; i < high; i++)
	{
		for (j = 0; j < width; j++)
		{
			if (feof(p))
			{
				printf("read over!");
				break;
			}
			else
			{
				if (fread(&(gray[i][j]), sizeof(char), 1, p) != 1)
				{
					printf("fatal error!");
					break;
				}
				if (gray[i][j] <= 200)gray[i][j] = 0;
				else gray[i][j] = 255;
			}
		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	/**********初始化保存数组***************/
	for (i = 0; i < 1000; i++)
	{
		for (j = 0; j < 1000; j++)
		{
			reserve[i][j] = 255;
		}
	}

	/************拷贝原数组值***********************/
	for (i = 0; i < high; i++)
	{
		for (j = 0; j < width; j++)
		{
			if (feof(p))
			{
				printf("read over!");
				break;
			}
			else
			{
				reserve[i][j] = gray[i][j];
			}
		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)


	/************拷贝结束**********************/
#if 0
	/*******************写回文件************************/
	fseek(p,gap,0);
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if(feof(p))
			{
				printf("read over!");
				break;
			}
			else
			{
				fwrite(&(reserve[i][j]),sizeof(char),1,p); 
			}
		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	/*****利用gray[][]判断连通区域个数***********/
#endif

	/******************/

	/*测试代码*/
#if 0
	FILE *f;
	f = fopen("D:\\Download\\a.txt", "w");
	for (int i = 0; i < high; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (gray[i][j] == 0){
				fprintf(f, "0");
			}
			else{
				fprintf(f, "1");
			}
		} //end of for(j=0;j<width;j++)
		fprintf(f, "\n");
	}  //end of (i=0;i<high;i++)
	fclose(f);
#endif
	fclose(p);

}