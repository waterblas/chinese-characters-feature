#include <string>

#ifndef GLOBLE
#define GLOBLE
#define M 6762//训练文件总数
extern int width;  //图像宽度(象素点数)
extern int high;   //图像高度(象素点数)
extern unsigned char gray[1000][1000];
extern int gap;   //数据起始位置
extern unsigned char reserve[1000][1000];
extern char filename_train[M][5];
extern int left,right,top,bottom;
extern unsigned char corrdinate_to_48[48][48];
extern float percent[16];  //保存corrdinate_to_48中投影围特征各行列的百分比
extern float percent_blanks[32];  //保存corrdinate_to_48中粗外围特征各行列的百分比
extern int total_piexls;   //corrdinate_to_48中总象素数
extern int total_blanks;   //corrdinate_to_48中总空白点数
extern int database_total[M]; //用于保存所有训练样本的总象素数
extern float database_percent[M][16];  //用于保存所有训练样本的投影象素百分比
extern float database_percent_blanks[M][32];  //用于保存所有训练样本的粗外围特征各行列空白百分比
extern int N;
extern int num_of_connection[M];  //所有训练样本连同区域的总数量
extern std::string filename_tail;
extern std::string filename_head;
#endif