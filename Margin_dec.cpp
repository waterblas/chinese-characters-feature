#include "Globle_Variable.h"
#include <stdio.h>
#include<malloc.h>

/****函数margin_dec( int& l,int& r,int& t,int& b)*********************/
/****函数功能是利用当前的全局数组reserve[][]对当前的样本进行边缘检测*************/
/*****函数返回值:用引用调用形式返回当前样本的四个边界值***********************/
void margin_dec(int& l, int& r, int& t, int& b)
{
	int i, j;
	l = r = t = b = 0;
	//char temp1,temp2;

	/************提取上边界***********************/
	for (i = 0; i < high; i++)
	{
		//if(t!=0) break;
		//	printf("num of top is%d\n",top);
		for (j = 0; j < width; j++)
		{
			if (t != 0) break;
			if ((reserve[i][j] == 0) && ((reserve[i][j + 1] == 0) || (reserve[i + 1][j - 1] == 0) || (reserve[i + 1][j] == 0) || (reserve[i + 1][j + 1] == 0)))

			{
				t = i;
				break;
			}
		} //end of for(j=0;j<width;j++)
	}  //end of (i=0;i<high;i++)
	/************提取上边界结束**********************/

	/************提取下边界***********************/
	for (i = high - 1; i >= 0; i--)
	{
		if (b != 0) break;
		for (j = 0; j < width; j++)
		{

			if ((reserve[i][j] == 0) && ((reserve[i][j + 1] == 0) || (reserve[i - 1][j - 1] == 0) || (reserve[i - 1][j] == 0) || (reserve[i - 1][j + 1] == 0)))
			{
				b = i;
				break;
			}
		} //end of for(j=0;j<width;j++)
	}  //end of (i=0;i<high;i++)
	/************提取下边界结束**********************/

	/************提取左边界***********************/

	for (i = 0; i < width; i++)
	{
		//if(t!=0) break;
		//	printf("num of top is%d\n",top);

		for (j = 0; j < high; j++)  //按列扫描
		{
			if (l != 0) break;
			if ((reserve[j][i] == 0) && ((reserve[j + 1][i] == 0) || (reserve[j - 1][i + 1] == 0) || (reserve[j][i + 1] == 0) || (reserve[j + 1][i + 1] == 0)))
			{
				l = i;
				break;
			}

		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)


	/************提取左边界结束**********************/

	/************提取右边界***********************/

	for (i = width - 1; i >= 0; i--)
	{
		//if(t!=0) break;
		//	printf("num of top is%d\n",top);
		for (j = 0; j < high; j++)  //按列扫描
		{
			if (r != 0) break;
			if ((reserve[j][i] == 0) && ((reserve[j + 1][i] == 0) || (reserve[j - 1][i - 1] == 0) || (reserve[j][i - 1] == 0) || (reserve[j + 1][i - 1] == 0)))
			{
				r = i;
				break;
			}
		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	/************提取右边界结束**********************/
}
