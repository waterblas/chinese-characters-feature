#include "Globle_Variable.h"
#include <stdio.h>
/****函数名corrdinate()**************************/
/****函数功能: 利用二值化拷贝数组reserve[][]将图像归一化成48*48大小,
				   对应象素存入全局数组corrdinate_to_48[][]中
				   并且写入一个bmp文件 ********************/
void corrdinate()
{
	int i, j;
	int gap;
	//char temp1,temp2;
	FILE *q;   //读调整后位图文件指针用于读写
	if (!(q = fopen("backup.bmp", "rb+")))
		printf("can't not open bmp file");
	fseek(q, 10, 0);
	fread(&gap, sizeof(int), 1, q);    //读数据起始位置信息

	int kx = (right - left);
	int ky = (bottom - top);

	/***********************/
	for (i = 0; i < 48; i++)
	{
		for (j = 0; j < 48; j++)
		{
			corrdinate_to_48[i][j] = 255;
		}
	}
	/*********************/

	//#if 0
	/************提取上边界***********************/
	/* for(i=top;i<=bottom;i++)
	 {
	 //if(t!=0) break;
	 //	printf("num of top is%d\n",top);

	 for(j=left;j<=right;j++)
	 {

	 if(reserve[i][j]==0)

	 {
	 corrdinate_to_48[(i-top)*47/ky][(j-left)*47/kx]=0;
	 }

	 } //end of for(j=0;j<width;j++)

	 }  //end of (i=0;i<high;i++)

	 */
	for (i = 0; i <= 47; i++)
	{
		//if(t!=0) break;
		//	printf("num of top is%d\n",top);

		for (j = 0; j <= 47; j++)
		{

			// if(reserve[i][j]==0)

			//{
			// corrdinate_to_48[(i-top)*47/ky][(j-left)*47/kx]=0;
			corrdinate_to_48[i][j] = reserve[i*ky / 47 + top][j*kx / 47 + left];
			//	}

		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)


	/************提取上边界结束**********************/
	//#endif
	/*******************写回文件************************/
	fseek(q, gap, 0);
	for (i = 0; i < 48; i++)
	{
		for (j = 0; j < 48; j++)
		{
			if (feof(q))
			{
				printf("read over!");
				break;
			}
			else
			{
				fwrite(&(corrdinate_to_48[i][j]), sizeof(char), 1, q);
			}
		} //end of for(j=0;j<width;j++)

	}  //end of (i=0;i<high;i++)
	fclose(q);
}