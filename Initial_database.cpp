#include "Globle_Variable.h"
#include <stdio.h>
#include"Function_Declare.h"
void initial_database()
{
	int i, j;
	for (j = 0; j < M; j++)
	{
		N = j;
		/**********对所有训练样本二值化********************/
		two_value_train(N);
		/*************粗外围特征检测************************/
		margin_dec(left, right, top, bottom);
		//printf("num of t is%d\n",top);
		//printf("num of b is%d\n",bottom);
		//printf("num of l is%d\n",left);
		// printf("num of r is%d\n",right);
		/*************字大小调整至48*48大小************************/
		corrdinate();

		/*************将特征值写回数据库************************/
		total_piexls = Func_percent(percent);
		total_blanks = Func_percent_blank(percent_blanks);
		//printf("num oftotal_piexls is%d\n",total_piexls);
		database_total[j] = total_piexls;
		/**************将所有训练样本的投影百分比写入全局数组database_percent***********/
		for (i = 0; i < 16; i++)
		{
			database_percent[N][i] = percent[i];
		}
		/**************将所有训练样本的外围特征百分比写入全局数组database_percent_blanks***********/
		for (i = 0; i < 32; i++)
		{
			database_percent_blanks[N][i] = percent_blanks[i];
		}
		/*************写回数据库完毕!************************/
	}
}